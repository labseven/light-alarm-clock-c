#include <ESP8266WebServer.h>
#include <config.h>

ESP8266WebServer server(80);

String edit_alarm_html(struct Alarm, int);
String view_alarm_html(struct Alarm, int);

char* format_time(int time) {
  int hours = time / 60;
  int minutes = time % 60;
  char* output = "HH:MM";
  sprintf(output, "%d:%02d", hours, minutes);
  return output;
}

int parse_time(String time) {
  int index_to_cut = time.indexOf(':');
  if (index_to_cut == -1){
    index_to_cut = time.indexOf('.');
  }

  int hours = time.substring(0, index_to_cut).toInt();
  int minutes = time.substring(index_to_cut+1).toInt();

  Serial.print(time.indexOf(':'));
  Serial.print(" ");
  Serial.print(hours);
  Serial.print(" ");
  Serial.print(minutes);
  Serial.print(" ");
  Serial.print(time.substring(index_to_cut + 1));
  Serial.println("{");

  return hours * 60 + minutes;
};



bool set_variables_from_form(String key, String value, struct Alarm * alarm) {
  if (key == "alarm_time") { alarm->time = parse_time(value); }
  if (key == "alarm_fade_in_mins") { alarm->fade_in_mins = value.toInt(); }
  if (key == "alarm_keep_on_mins") { alarm->keep_on_mins = value.toInt(); }
  if (key == "alarm_max_brightness") { alarm->max_brightness = value.toInt(); }
  if (key == "alarm_enabled") { alarm->enabled = true; }
  if (key == "weekdays") { alarm->weekdays[value.toInt()] = true; }

  return true;
}

void check_authentication() {
  if (www_password[0] == 0) {
    return;
  }
  if (!server.authenticate(www_username, www_password)) {
    return server.requestAuthentication();
  }
}

void setupHTTPServer() {
  server.on("/", []{
    check_authentication();

    String html = "<!DOCTYPE html><body><table>";
    html = html + "<tr><th>Alarm Time</th><th>Days On</th><th>Fade In</th><th>Keep On</th><th>Max Brightness</th><th>Enabled</th><th>Edit</th></tr>";
    for (int i=0; i<NUM_ALARMS; i++) {
      html = html + "<tr>" + view_alarm_html(alarms[i], i) + "</tr>";
    }
    html = html + "</table></body>";

    server.send(200, "text/html",html);

  });

  server.on("/edit", []{
    check_authentication();
    if(server.args() == 1 && server.argName(0) == "alarm" && server.arg(0).toInt() < NUM_ALARMS) {
      server.send(200, "text/html", edit_alarm_html(alarms[server.arg(0).toInt()], server.arg(0).toInt()));
    }
    else {
      server.send(404, "text/plain", "cannot edit this");
    }
  });

  server.on("/post", []{
    check_authentication();

    if(server.argName(0) == "alarm" && server.arg(0).toInt() < NUM_ALARMS) {
      struct Alarm * alarm = &alarms[server.arg(0).toInt()];

      // Checkbox only shows up (with value 0) if it is checked
      alarm -> enabled = false;
      for (int i = 0; i<7; i++) {
        alarm -> weekdays[i] = false;
      }
      for (uint8_t i = 0; i < server.args(); i++) {
        Serial.println(server.argName(i) + ": " + server.arg(i));
        set_variables_from_form(server.argName(i), server.arg(i), alarm);
      }

      Serial.println("redirecting to /");
      server.sendHeader("Location", "/");
      server.send(302);

      Serial.println("Saving alarm to EEPROM");
      save_alarms_to_eeprom();
    }
    else {
      server.send(404, "text/plain", "cannot edit this");
    }

  });

  server.begin();
}

void pollHTTPServer() {
  server.handleClient();
}

String view_alarm_html(struct Alarm alarm, int alarm_num) {
  String html = "<tr>";
  html = html + "<td>" + (alarm.enabled ? "<b>" : "<em>") + format_time(alarm.time) + (alarm.enabled ? "</b>" : "</em>") + "</td>";

  html = html + "<td>";
  html = html + (alarm.weekdays[1] ? "M" : "_") + " ";
  html = html + (alarm.weekdays[2] ? "T" : "_") + " ";
  html = html + (alarm.weekdays[3] ? "W" : "_") + " ";
  html = html + (alarm.weekdays[4] ? "T" : "_") + " ";
  html = html + (alarm.weekdays[5] ? "F" : "_") + " ";
  html = html + (alarm.weekdays[6] ? "S" : "_") + " ";
  html = html + (alarm.weekdays[0] ? "S" : "_") + " ";
  html = html + "</td>";

  html = html + "<td>" + alarm.fade_in_mins + "</td>";
  html = html + "<td>" + alarm.keep_on_mins + "</td>";
  html = html + "<td>" + alarm.max_brightness + "</td>";
  html = html + "<td>" + (alarm.enabled ? "enabled" : "disabled") + "</td>";

  html = html + "<td><a href=/edit?alarm=" + alarm_num + ">Edit Alarm " + alarm_num + "</a></td>";

  html = html + "</tr>";

  return html;
}

String edit_alarm_html(struct Alarm alarm, int alarm_num) {
  return "<!DOCTYPE html>\
    <html><head></head><body><link rel=\"icon\" href=\"data:;base64,=\"><h1>Edit Alarm:</h1>\
    <form method=\"post\" action=/post?alarm=" + String(alarm_num) + ">\
      <table>\
      <tr>\
  <td><span title=\"Tip: use . as a seperator\">Time (24h):</span></td>\
    <td><input type=\"text\" name=\"alarm_time\" value=\"" + format_time(alarm.time) + "\"><br></td>\
    </tr>\
    <tr>\
    <td>Days:</td>\
      <td>\
        M: <input type=\"checkbox\" name=\"weekdays\" value=1 " + (alarm.weekdays[1] ? "checked" : "") + ">\
        T: <input type=\"checkbox\" name=\"weekdays\" value=2 " + (alarm.weekdays[2] ? "checked" : "") + ">\
        W: <input type=\"checkbox\" name=\"weekdays\" value=3 " + (alarm.weekdays[3] ? "checked" : "") + ">\
        T: <input type=\"checkbox\" name=\"weekdays\" value=4 " + (alarm.weekdays[4] ? "checked" : "") + ">\
        F: <input type=\"checkbox\" name=\"weekdays\" value=5 " + (alarm.weekdays[5] ? "checked" : "") + ">\
        S: <input type=\"checkbox\" name=\"weekdays\" value=6 " + (alarm.weekdays[6] ? "checked" : "") + ">\
        S: <input type=\"checkbox\" name=\"weekdays\" value=0 " + (alarm.weekdays[0] ? "checked" : "") + ">\
      </td>\
      </tr>\
      <tr>\
  <td>Fade In (mins):</td>\
    <td><input type=\"text\" name=\"alarm_fade_in_mins\" value=\"" + alarm.fade_in_mins + "\"><br></td>\
    </tr>\
  <td>Keep On (mins):</td>\
    <td><input type=\"text\" name=\"alarm_keep_on_mins\" value=\"" + alarm.keep_on_mins + "\"><br></td>\
    </tr>\
  <td>Max Brightness (0-255):</td>\
    <td><input type=\"text\" name=\"alarm_max_brightness\" value=\"" + alarm.max_brightness + "\"><br></td>\
    </tr>\
  <td>Enabled:</td>\
    <td><input type=\"checkbox\" name=\"alarm_enabled\" " + (alarm.enabled ? "checked" : "") + "><br></td>\
    </tr>\
      <tr>\
      <td><a href=/>back</a></td><td><br><input type=\"submit\" value=\"Submit\"></td>\
      </tr>\
    </form>\
    </body></html>";
}
