#ifndef CONFIG_H_INCLUDED
#define CONFIG_H_INCLUDED

#define NUM_ALARMS 2 // Change this to the number of alarms you want

extern const char* ssid;
extern const char* password;

extern char* local_hostname;

extern const char* www_username;
extern const char* www_password;

extern int led_pin;
extern const bool invert_LED;

struct Alarm {
  int time;
  bool weekdays[7];
  int fade_in_mins;
  int keep_on_mins;
  int max_brightness;
  bool enabled;
};

extern struct Alarm alarms[NUM_ALARMS];

extern void load_alarms_from_eeprom();
extern void save_alarms_to_eeprom();

#endif
