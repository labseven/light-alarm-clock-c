#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WiFiUdp.h>

#include <NTPClient.h>
#include <EEPROM.h>
// #include <avr/eeprom.h>

#include <ArduinoOTA.h>

#include <Time.h>
#include <TimeLib.h>
#include <Timezone.h>

#include <config.h>
#include <ota.h>
#include <HTTPServer.h>

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", 0, 6 * 60 * 60 * 1000); // 6 hours ntp update interval

WiFiClient client;

String date;
String t;
String tempC;
const char * days[] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"} ;
const char * months[] = {"Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"} ;
const char * ampm[] = {"AM", "PM"} ;


void setup() {
  Serial.begin(115200);

  EEPROM.begin(512);

  WiFi.begin(ssid, password);
  while ( WiFi.status() != WL_CONNECTED ) {
    delay ( 500 );
    Serial.print ( "." );
  }

  timeClient.begin();

  setupArduinoOTA(local_hostname);
  setupHTTPServer();

  Serial.print(WiFi.localIP());
  Serial.print(" on ");
  Serial.println(ssid);


  load_alarms_from_eeprom();
}




void tellTime() {
  date = "";  // clear the variables
  t = "";


  timeClient.update();
  unsigned long epochTime =  timeClient.getEpochTime();

  // convert received time stamp to time_t object
  time_t local, utc;
  utc = epochTime;

  // Then convert the UTC UNIX timestamp to local time
  // TimeChangeRule usPDT = {"PDT", Second, Sun, Mar, 2, -420};  //UTC - 7 hours
  // TimeChangeRule usPST = {"PST", First, Sun, Nov, 2, -480};   //UTC - 8 hours
  // Timezone usPacific(usPDT, usPST);
  // local = usPacific.toLocal(utc);

  TimeChangeRule usEDT = {"EDT", Second, Sun, Mar, 2, -240};  //UTC - 4 hours
  TimeChangeRule usEST = {"EST", First, Sun, Nov, 2, -300};   //UTC - 5 hours
  Timezone usEastern(usEDT, usEST);
  local = usEastern.toLocal(utc);

  // now format the Time variables into strings with proper names for month, day etc
  date += days[weekday(local) - 1];
  date += ", ";
  date += months[month(local) - 1];
  date += " ";
  date += day(local);
  date += ", ";
  date += year(local);

  // format the time to 12-hour format with AM/PM and no seconds
  t += hourFormat12(local);
  t += ":";
  if (minute(local) < 10) // add a zero if minute is under 10
    t += "0";
  t += minute(local);
  t += " ";
  t += ampm[isPM(local)];

  // Display the date and time
  Serial.println("");
  Serial.print("Local date: ");
  Serial.print(date);
  Serial.println("");
  Serial.print("Local time: ");
  Serial.println(t);
}


int led_fade_in_brightness(int today_in_seconds, struct Alarm alarm) {
  // Eyes do not preceive light linearly, ramp this up with an exponent (note: https://diarmuid.ie/blog/pwm-exponential-led-fading-on-arduino-or-other-platforms/)
  float fade_in_ratio = 1.0 * (today_in_seconds - ((alarm.time * 60) - (alarm.fade_in_mins * 60))) / (alarm.fade_in_mins * 60);

  Serial.print(fade_in_ratio);

  return max(1, (int)(alarm.max_brightness * pow(fade_in_ratio, 6)));
}

void checkTime(){
  timeClient.update();
  unsigned long epochTime =  timeClient.getEpochTime();

  // convert received time stamp to time_t object
  time_t local, utc;
  utc = epochTime;

  // Then convert the UTC UNIX timestamp to local time
  // TimeChangeRule usPDT = {"PDT", Second, Sun, Mar, 2, -420};  //UTC - 7 hours
  // TimeChangeRule usPST = {"PST", First, Sun, Nov, 2, -480};   //UTC - 8 hours
  // Timezone usPacific(usPDT, usPST);
  // local = usPacific.toLocal(utc);

  TimeChangeRule usEDT = {"EDT", Second, Sun, Mar, 2, -240};  //UTC - 4 hours
  TimeChangeRule usEST = {"EST", First, Sun, Nov, 2, -300};   //UTC - 5 hours
  Timezone usEastern(usEDT, usEST);
  local = usEastern.toLocal(utc);

  int led_value;
  if (!invert_LED) { led_value = 0;   }
  else             { led_value = 255; }

  for (int i = 0; i<NUM_ALARMS; i++){
    struct Alarm alarm = alarms[i];

    Serial.print("Alarm ");
    Serial.println(i);

    if (alarm.enabled && alarm.weekdays[weekday(local) - 1]) {
      int today_in_minutes = hour(local) * 60 + minute(local);
      int today_in_seconds = today_in_minutes * 60 + second(local);

      if (today_in_minutes < alarm.time - alarm.fade_in_mins) {
        Serial.print("minutes remain until alarm: ");
        Serial.println(alarm.time - alarm.fade_in_mins - today_in_minutes);
      }
      else if (today_in_minutes < alarm.time) {
        Serial.println("Alarm!");
        Serial.print("minutes remain: ");
        Serial.print(alarm.time - today_in_minutes - 1);
        Serial.print(":");
        Serial.println(60 - second(local));

        if (!invert_LED) {
          led_value = max(led_value, led_fade_in_brightness(today_in_seconds, alarm));
        } else {
          led_value = min(led_value, 255 - led_fade_in_brightness(today_in_seconds, alarm));
        }
        Serial.print("LED value: ");
        Serial.println(led_value);
      }
      else if (today_in_minutes < alarm.time + alarm.keep_on_mins) {
        Serial.print("Staying on: ");
        Serial.println(alarm.time + alarm.keep_on_mins - today_in_minutes);

        if (!invert_LED) {
          led_value = max(led_value, alarm.max_brightness);
        } else {
          led_value = min(led_value, 255 - alarm.max_brightness);
        }
      }
      else {
        Serial.println("Alarm over today");
      }
    }
    else if (!alarm.enabled){
      Serial.println("Alarm Disabled");
    }
    else if (!alarm.weekdays[weekday(local) - 1]) {
      Serial.println("Alarm Off Today");
    }

    Serial.print("LED value: ");
    Serial.println(led_value);

    analogWrite(led_pin, led_value);
  }
}


void loop() {
  delay(1000);
  tellTime();
  checkTime();

  pollHTTPServer();

  delay(100);
}
