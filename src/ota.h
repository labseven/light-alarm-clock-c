#ifndef OTA_H_INCLUDED
#define OTA_H_INCLUDED
void setupArduinoOTA(char* hostname);
void updateArduinoOTA();
#endif
