# ESP8266 Light Alarm Clock

This is an IoT light alarm clock.


## Install:

Compile and upload with [platformIO](https://platformio.org/) (Install instruction [here](https://platformio.org/install/cli)).

Copy `src/config.cpp.example` to `src/config.cpp` and modify the settings (also set the number of alarms in `src/config.h`).

Plug in the ESP8266 and run:
```platformio run --target upload ;and platformio device monitor -b 115200```

Connect to it with a browser (your_hostname.local) to set your alarms.

Finally connect LEDs to an output pin (through a transistor) and wake up to a gentle light.


## Todo:

* ~~Make single alarm work~~
* ~~Make settings html page to update alarm settings~~
* ~~Add weekdays and enabled to settings webpage~~
* ~~Make settings read/write from EEPROM~~
* ~~Make multiple alarms~~
* ~~Change algorithm for ramping up brightness (eyes do not preceive linear)~~
* ~~Make brightness update every second (currently only uses minutes)~~
* ~~Make time input/output human-nice HH:MM~~
* ~~No password prompt if admin pass is blank~~
* Add CSS to pages?
* Option for blinking?
* Draw schematic for wiring
* Clean up the code :)
