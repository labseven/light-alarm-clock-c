# import sys
# user_input = sys.stdin.readlines()

user_input = """
int alarm_time = 16 * 60 + 0;
int alarm_fade_in_mins = 10;
int alarm_keep_on_mins = 5;
int alarm_max_brightness = 1023;
bool alarm_enabled = true;
"""

# def clean_name(name):
#   return name.translate({ord(i):None for i in '[]0123456789'})


variables = []

for line in user_input.splitlines():
  line = line.split()
  if len(line) > 1:
    variables.append({"var_type": line[0], "var_name": line[1]})


print("input: ")
for var in variables:
  print(var['var_type'], var['var_name'])


print()
print("variable setting:")

for var in variables:
  vName = var['var_name']
  vType = var['var_type']
  if vType == "int":
    print("""  if (key == "{}") {{ {} = value.toInt(); }}""".format(vName, vName))
  elif vType == "bool":
    print("""  if (key == "{}") {{ {} = (value == "true"); }}""".format(vName, vName))
  else:
    print("ERROR: {} not supported".format(vType))

print()
print("html:")

for var in variables:
  vName = var['var_name']
  vType = var['var_type']
  print("""<td>{}:</td>\\
  <td><input type=\\"text\\" name=\\"{}\\" value=\\"" + {} + "\\"><br></td>\\
  </tr>\\""".format(vName, vName, vName))

